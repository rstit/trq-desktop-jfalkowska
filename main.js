const path = require('path');
const {app, BrowserWindow} = require('electron');
const url = require('url');
const server = require('./src/backend/server');

const browserWindowOptions = {
  development: {
    pathname: 'localhost:4200',
    protocol: 'http:',
    slashes: true
  },
  production: {
    pathname: path.join(__dirname, 'dist', 'index.html'),
    protocol: 'file:',
    slashes: true
  }
};

const isDev = process.env.NODE_ENV === 'development';
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

async function createWindow () {
  // We need to be sure that everything is initialized first, before we can create an UI.
  await server.init();

  // Create the browser window.
  win = new BrowserWindow({width: 1024, height: 768, frame: false})
  const options = isDev ? browserWindowOptions.development : browserWindowOptions.production;
  // and load the Angular app.
  win.loadURL(url.format(options));

  if (isDev) {
    // Dev tools needs to be detached because of the custom titlebar.
    // Window with attached dev tools could not be dragged with custom titlebar.
    win.webContents.openDevTools({
      mode: 'detach'
    });
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  app.quit()
});

const shouldQuit = app.makeSingleInstance((commandLine, workingDirectory) => {
  // Someone tried to run a second instance, we should focus our window.
  if (win) {
    if (win.isMinimized()) {
      win.restore();
    }
    win.focus();
  }
});

if (shouldQuit) {
  app.quit();
  return;
}
