'use strict';
module.exports = (sequelize, DataTypes) => {
  var Sequence = sequelize.define('sequence', {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    }
  }, {});
  Sequence.associate = function(models) {
    // associations can be defined here
    models.sequence.hasMany(models.scan);
  };
  return Sequence;
};
