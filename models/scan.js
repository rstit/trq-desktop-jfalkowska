'use strict';
module.exports = (sequelize, DataTypes) => {
  var Scan = sequelize.define('scan', {
    partNumber: DataTypes.STRING,
    manufacturer: DataTypes.STRING,
    description: DataTypes.STRING,
    detectionDepth: DataTypes.NUMERIC,
    frequency: DataTypes.NUMERIC,
    additionalInfo: DataTypes.JSON
  }, {});
  Scan.associate = function(models) {
    // associations can be defined here
    models.scan.belongsTo(models.sequence);
    models.scan.belongsTo(models.material);
  };
  return Scan;
};
