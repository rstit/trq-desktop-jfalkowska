const device = require('./device');

const {
  DEVICE_PORT,
  FAKE_DEVICE_HOST,
 } = require('../src/backend/constants');

const isHeadlessMode = process.argv.includes('--headless');

if (!isHeadlessMode) {
  const ui = require('./ui');
  ui.toggleScanClicked = () => {
    const { triggerPressed } = device.getState();
    if (triggerPressed) {
      device.releaseTrigger();
    } else {
      device.pressTrigger();
    }
    const newState = device.getState();
    ui.setState(newState);
  }

  device.on('state', (state) => {
    ui.setState(state);
  });

  ui.disconnectClicked = () => {
    device.disconnect();
  }

  device.onOutput((buffer, date) => {
    ui.addOutput(buffer, date);
  });

  device.onInput((buffer, date) => {
    ui.addInput(buffer, date);
  });

  ui.setState(device.getState());
  device.listen(DEVICE_PORT, FAKE_DEVICE_HOST);
} else {
  console.log(`Fake Device TCP @ ${FAKE_DEVICE_HOST}:${DEVICE_PORT}`);
}

// Exporting the device for e2e tests.
module.exports = device;
