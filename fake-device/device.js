const net = require('net');
const logger = require('./logger');
const protocol = require('../src/backend/communication/protocol');

const {
  DEVICE_ID,
  DEVICE_FIRMWARE_VERSION,
  STATUS_WRONG_DATA_VALUES,
  BEEP_HEADER,
  FUNCTION_BEEP,
  READ_DATA_HEADER,
  WRITE_DATA_HEADER,
  DDS_CONFIG_STRUCT,
  HANDSHAKE,
  STATUS_OK,
  BATTERY_STATUS_STRUCT,
  ADC,
 } = require('../src/backend/constants');


const MEASURE_INTERVAL_MS = 75;
const BATTERY_INTERVAL_MS = 2000;

const createState = () => ({
  connected: false,
  frequencyTuning: 0,
  delta: 0,
  triggerPressed: false,
});
const DEVICE_START_TIME = Date.now();

const state = createState();
const setState = newState => {
  Object.assign(state, newState);
  device.emit('state', state);
};
const getState = () => state;

const handleBeep = (buffer, socket) => {
  const { result } = protocol.read(buffer).beep();
  if (result.function === FUNCTION_BEEP) {
    const beep = {
      status: STATUS_OK,
    };
    const message = protocol.write().beepResponse(beep).result;
    socket.write(message);
  }
};

const handleReadData = (buffer, socket) => {
  const { result } = protocol.read(buffer).readData();
  switch(result.struct_id) {
    case HANDSHAKE:
      const handshake = {
        struct_id: HANDSHAKE,
        struct: {
          deviceId: DEVICE_ID,
          firmwareVersion: DEVICE_FIRMWARE_VERSION,
          deviceStatus: STATUS_OK,
        },
        status: STATUS_OK
      };
      const message = protocol.write().readDataResponse(handshake).result;
      socket.write(message);
      return;
  }
}

const handleWriteData = (buffer, socket) => {
  const { result } = protocol.read(buffer).writeData();
  switch(result.struct_id) {
    case DDS_CONFIG_STRUCT:
      if (result.struct.frequencyTuning < 0) {
        const error = protocol.write().writeDataResponse({
          status: STATUS_WRONG_DATA_VALUES,
        }).result;
        socket.write(error);
        return;
      }
      setState({
        frequencyTuning: result.struct.frequencyTuning,
      });
      return;
  }
}

const sendMeasureDataInIntervals = (socket) => {
  setInterval(() => {
    if (!state.connected || !state.triggerPressed || socket.destroyed) {
      return;
    }
    const measurementsCount = 10;
    const values = [];
    for (let i = 0; i < measurementsCount; i++) {
      values.push(Math.round(Math.random() * 255));
    }
    const measure = {
      timestamp: Date.now() - DEVICE_START_TIME,
      delta: MEASURE_INTERVAL_MS,
      count: measurementsCount,
      type: ADC,
      values
    };
    const message = protocol.write().measureData(measure).result;
    socket.write(message);
  }, MEASURE_INTERVAL_MS);
};

const sendBatteryStatusInIntervals = (socket) => {
  let token = setInterval(() => {
    if (socket.destroyed) {
      clearInterval(token);
      return;
    }
    const batteryLevel = Math.round(Math.random() * 100);
    const batteryStatusMessage = protocol.write().readDataResponse({
      struct_id: BATTERY_STATUS_STRUCT,
      struct: {
        batteryLevel,
      },
    }).result;
    socket.write(batteryStatusMessage);
  }, BATTERY_INTERVAL_MS);
}

let client;
const device = net.createServer(socket => {
  client = socket;
  setState({
    connected: true
  })
  logger.decorateWriteWithLogger(socket);
  socket.on('data', buffer => {
    logger.logInput(buffer);
    switch(buffer[0]) {
      case BEEP_HEADER:
        handleBeep(buffer, socket);
        return;
      case READ_DATA_HEADER:
        handleReadData(buffer, socket);
        return;
      case WRITE_DATA_HEADER:
        handleWriteData(buffer, socket);
        return;
    }
  });
  sendBatteryStatusInIntervals(socket);
  sendMeasureDataInIntervals(socket);
});

device.pressTrigger = () => {
  setState({
    triggerPressed: true,
  });
}

device.releaseTrigger = () => {
  setState({
    triggerPressed: false,
  });
}

device.disconnect = () => {
  if (client) {
    client.destroy();
    setState({
      connected: false
    });
  }
}

device.getCurrentFrequency = () => {
  return new Promise((resolve) => {
    // Lets wait for any lags on the network
    // (that device can run on localhost, but doesn't now have to).
    setTimeout(() => {
      const state = getState();
      resolve(state.frequencyTuning);
    }, 250);
  });
}

device.setState = setState;
device.getState = getState;

device.onOutput = (callback) => {
  logger.registerOutputHandler(callback);
}

device.onInput = (callback) => {
  logger.registerInputHandler(callback);
}

module.exports = device;
