const Navigation = require('./Navigation');

module.exports = class ScanSetup extends Navigation {
  constructor(app) {
    super(app);
  }

  static getIndexFromUnit(unit) {
    switch(unit) {
      case 'Hz' : return 0;
      case 'kHz': return 1;
      case 'MHz': return 2;
    }
  }

  waitUntilConnected() {
    return this.app.client.waitUntil(async () => {
      const text = await this.app.client.getText('main h1');
      return text === 'Scanner Connected!';
    }, 60000, 'Cannot connect to device in time');
  }

  async isCurrentStepOfTotal(current, total) {
    const text = await this.app.client.getText('h3.setup-current-step');
    return text === `Scan Setup: Step ${current}/${total}`;
  }

  selectFirstMaterialOnList() {
    return this.app.client.click('trq-materials-list-view .material-tile:first-child');
  }

  async fillDetectionDepth(detectionDepth) {
    return this.app.client.setValue('trq-scan-setup input[name="detectionDepth"]', detectionDepth);
  }

  async confirmFrequency(frequency) {
    const selector = 'trq-scan-setup button[type="submit"]';
    const button = await this.app.client.element(selector);
    const text = await this.app.client.elementIdText(button.value.ELEMENT);
    if (text.value === 'Confirm') {
      return this.app.client.click(selector);
    } else {
      throw new Error('Confirm button not found!');
    }
  }

  async clickEditFrequencyButton() {
    const selector = 'trq-scan-setup button.btn-secondary';
    const button = await this.app.client.element(selector);
    const text = await this.app.client.elementIdText(button.value.ELEMENT);
    if (text.value === 'Edit') {
      return this.app.client.click(selector);
    } else {
      throw new Error('Edit button not found!');
    }
  }

  async toggleFrequencyUnitDropdown() {
    this.app.client.click('trq-scan-setup .dropdown-current');
    return this.app.client.waitUntil(async () => {
      return this.app.client.isVisible('trq-scan-setup .dropdown-options-list');
    });
  }

  async selectOptionInFrequencyUnitDropdown(index) {
    await this.app.client.click(`trq-scan-setup .dropdown-options-list .dropdown-option:nth-child(${index + 1})`)
  }

  async changeFrquencyUnit(unit) {
    const index = ScanSetup.getIndexFromUnit(unit);
    await this.toggleFrequencyUnitDropdown();
    await this.selectOptionInFrequencyUnitDropdown(index);
  }

  fillFrequency(frequency) {
    return this.app.client.setValue('trq-scan-setup input[name="frequency"]', frequency);
  }

  fillPartNumber(partNumber) {
    return this.app.client.setValue('trq-scan-setup input[name="partNumber"]', partNumber);
  }

  fillDescription(description) {
    return this.app.client.setValue('trq-scan-setup textarea[name="description"]', description);
  }

  submitScanStep() {
    return this.app.client.click('trq-scan-setup button[type="submit"]');
  }

  isMaterialSelected() {
    return this.app.client.isVisible('trq-materials-list-view .material-tile.is-selected');
  }

  async isDetectionDepthFilled() {
    return (await this.app.client.getValue('trq-scan-setup input[name="detectionDepth"]')).trim() !== '';
  }

  async isFrequencyFilled() {
    return (await this.app.client.getValue('trq-scan-setup input[name="frequency"]')).trim() !== '';
  }

  async isPartNumberFilled() {
    return (await this.app.client.getValue('trq-scan-setup input[name="partNumber"]')).trim() !== '';
  }

  async isDescriptionFilled() {
    return (await this.app.client.getValue('trq-scan-setup textarea[name="description"]')).trim() !== '';
  }
}
