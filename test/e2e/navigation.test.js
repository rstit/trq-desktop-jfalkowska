const { expect } = require('chai');
const testUtils = require('./utils');
const Navigation = require('./Navigation');

describe('Navigation', () => {
  beforeEach(testUtils.beforeEach(Navigation));
  afterEach(testUtils.afterEach());
  it('does have a working back button', async function () {
    // It's invisible on main page
    const isBackButtonVisibleFirstScreen = await this.page.isBackButtonVisible();
    expect(isBackButtonVisibleFirstScreen).to.equal(false);

    // Lets change a page to scan history and see if there is a back button
    await this.page.gotoScanHistory();
    const isBackButtonVisibleOnScanHistory = await this.page.isBackButtonVisible();
    expect(isBackButtonVisibleOnScanHistory).to.equal(true);

    // lets get back to previous screen, back button should hive
    await this.page.goBack();
    const isBackButtonVisibleOnPreviousScreen = await this.page.isBackButtonVisible();
    expect(isBackButtonVisibleOnPreviousScreen).to.equal(false);

    // Lets change a page to materials and see if there is a back button
    await this.page.gotoMaterials();
    const isBackButtonVisibleOnMaterials = await this.page.isBackButtonVisible();
    expect(isBackButtonVisibleOnMaterials).to.equal(true);

    // Lets again go to scan history
    await this.page.gotoScanHistory();

    // And hit first time the back button
    await this.page.goBack();

    // button should still be visible
    const isBackButtonVisibleAfterFirstBack = await this.page.isBackButtonVisible();
    expect(isBackButtonVisibleAfterFirstBack).to.equal(true);

    // And hit the back button for the second time
    await this.page.goBack();

    // button should still not be visible
    const isBackButtonVisibleAfterSecondBack = await this.page.isBackButtonVisible();
    expect(isBackButtonVisibleAfterSecondBack).to.equal(false);
  });

  it('can change screens', async function() {
    // On start we are on connection screen
    const isConnection = await this.page.isConnection();
    expect(isConnection).to.equal(true);

    // We can change screen to Scan History
    await this.page.gotoScanHistory();
    const isScanHistory = await this.page.isScanHistory();
    expect(isScanHistory).to.equal(true);

    // We can change screen to Materials
    await this.page.gotoMaterials();
    const isMaterials = await this.page.isMaterials();
    expect(isMaterials).to.equal(true);
  });

  it('can expend and collapse navigation pane', async function () {
    // By default it's collapsed
    const isNavigationCollapsedByDefault = await this.page.isNavigationCollapsed();
    expect(isNavigationCollapsedByDefault).to.equal(true);

    // Lets expand it
    await this.page.toggleNavigation();
    const isNavigationExpanded = await this.page.isNavigationExpanded();
    expect(isNavigationExpanded).to.equal(true);

    // Now lets check if we can collapse it again
    await this.page.toggleNavigation();
    const isNavigationCollapsed = await this.page.isNavigationCollapsed();
    expect(isNavigationCollapsed).to.equal(true);
  });
});
