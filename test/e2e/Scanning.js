const ScanSetup = require('./ScanSetup');

module.exports = class Scanning extends ScanSetup {
  constructor(app) {
    super(app);
  }

  async setupScan() {
    await this.selectFirstMaterialOnList();
    await this.submitScanStep();
    await this.fillDetectionDepth(1000);
    await this.submitScanStep();
    await this.confirmFrequency();
    await this.submitScanStep();
    await this.fillPartNumber('TRQ-1000');
    await this.submitScanStep();
    await this.fillDescription('Scan description');
    await this.submitScanStep();
  }

  async isWaitingForMeasurementsFramesFromDevice() {
    const result = await Promise.all([
      await this.app.client.isVisible('.scanning-process-idle'),
      await this.app.client.isVisible('.scanning-process-in-progress')
    ]);
    return result[0] && !result[1];
  }

  async getCurrentScansList() {
    const onElements = (items) => items.map(item => item.textContent);
    return await this.app.client.selectorExecute('.scanning-process-list-item', onElements);
  }

  async getDataFromFooter() {
    const results = await Promise.all([
      await this.app.client.getText('.scanning-process-scan header h2'),
      await this.app.client.selectorExecute('.scanning-process dd', (dd) => {
        const items = dd.map(item => item.textContent);
        return {
          description:    items[0],
          manufacturer:   items[1],
          additionalInfo: items[2],
          permeability:   items[3],
          detectionDepth: items[4],
          resistance:     items[5],
          frequency:      items[6],
        }
      }),
    ]);
    return {
      partNumber: results[0],
      ...results[1]
    };
  }
}
