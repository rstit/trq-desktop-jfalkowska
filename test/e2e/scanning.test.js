const { expect } = require('chai');
const testUtils = require('./utils');
const Scanning = require('./Scanning');

describe('Scanning', () => {
  beforeEach(testUtils.beforeEach(Scanning));
  afterEach(testUtils.afterEach());

  it('scanning process works', async function () {
    this.timeout(60000);
    // First we need to wait for the device to connect
    await this.page.waitUntilConnected();
    await this.page.gotoScanSetup();
    await this.page.setupScan();

    // We should be in idle mode: no measurements frames comming in.
    // There should be a partNumber on sans list already
    // and footer should have all propert informations about
    // the scan that will be performed.
    expect(await this.page.isWaitingForMeasurementsFramesFromDevice()).to.eq(true);
    expect(await this.page.getCurrentScansList()).to.deep.eq(['TRQ-1000']);
    expect(await this.page.getDataFromFooter()).to.deep.eq({
      partNumber: 'TRQ-1000',
      description: 'Scan description',
      manufacturer: '-',
      additionalInfo: '-',
      permeability: '123 µ',
      detectionDepth: '1000 µm',
      resistance: '234324 Ω / cm',
      frequency: '123 Hz',
    });

    // Lets start the scanning and wait 2s, so we can collect some measurements
    await this.device.pressTrigger();
    await this.page.wait2s();

    // We shuld be in scanning process:
    expect(await this.page.isWaitingForMeasurementsFramesFromDevice()).to.eq(false);
    // There still is only one element on scan list.
    expect(await this.page.getCurrentScansList()).to.deep.eq(['TRQ-1000']);

    // And lets stop the scanning process.
    await this.device.releaseTrigger();
    await this.page.wait1s();

    // We should now go back to beign in idle mode and on the scanning list there should be now two items.
    expect(await this.page.isWaitingForMeasurementsFramesFromDevice()).to.eq(true);
    expect(await this.page.getCurrentScansList()).to.deep.eq(['TRQ-1000 (1)', 'TRQ-1000']);
    expect(await this.page.getDataFromFooter()).to.deep.eq({
      partNumber: 'TRQ-1000 (1)',
      description: 'Scan description',
      manufacturer: '-',
      additionalInfo: '-',
      permeability: '123 µ',
      detectionDepth: '1000 µm',
      resistance: '234324 Ω / cm',
      frequency: '123 Hz',
    });
  });
});
