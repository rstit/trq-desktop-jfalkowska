const { expect } = require('chai');
const testUtils = require('./utils');
const Window = require('./Window');

describe('Application window', () => {
  beforeEach(testUtils.beforeEach(Window));
  afterEach(testUtils.afterEach());

  it('has proper title', async function () {
    const text = await this.page.getTitle();
    expect(text).to.equal('eddysense');
  });

  it('has no DevTool window opened', async function () {
    const isOpened = await this.page.isDevToolsOpened();
    expect(isOpened).to.equal(false);
  });

  it('can be minimalized and maximalized', async function () {
    const isInitiallyMaximized = await this.page.isMaximized();
    expect(isInitiallyMaximized).to.equal(false);

    await this.page.maximize();
    const isMaximized = await this.page.isMaximized();
    expect(isMaximized).to.equal(true);

    await this.page.minimize();
    const isMinimized = await this.page.isMinimized();
    expect(isMinimized).to.equal(true);
  });
});
