const tmp = require('tmp');
const Database = require('../../database/Database');
const MaterialModel = require('../../materials/material.model');
const MaterialProvider = require('../../materials/material.provider');

let materials;
let database;

const materialA = { name: 'a lorem' };
const materialB = { name: 'b impsum lorem' };
const materialC = { name: 'c ipsum' };

const database_location = tmp.tmpNameSync();

const setup = async () => {
  database = new Database(database_location);
  materials = new MaterialProvider({
    model: database.getModel('material'),
  });
  await database.authenticate();
  return await database.sync();
}

const tearDown = async () => {
  return await database.getModel('material').drop();
}

describe('materials', () => {
  beforeEach(setup);
  afterEach(tearDown);

  it('creates material', async () => {
    const name = 'fakeMaterial';
    const permeability = 0
    const resistance = 1
    const frequency = 2;
    const result = await materials.create({
      name,
      permeability,
      resistance,
      frequency,
    });
    expect(result.name).toEqual(name);
    expect(result.permeability).toEqual(permeability);
    expect(result.resistance).toEqual(resistance);
    expect(result.frequency).toEqual(frequency);
  });

  it('returns material by it\'s id', async () => {
    const name = 'fakeMaterial';
    const material = await materials.create({ name });
    const result = await materials.findById(material.id);
    expect(result.name).toEqual(name);
  });

  it('returns null if material with given id does not exist', async () => {
    const nonExistingMaterialId = 12345;
    const result = await materials.findById(nonExistingMaterialId);
    expect(result).toEqual(null);
  });

  it('can fetch multiple materials', async () => {
    await materials.create(materialA);
    await materials.create(materialB);
    await materials.create(materialC);
    const result = await materials.findAll();
    expect(result.length).toEqual(3);
    expect(result[0]).toMatchObject(materialA);
    expect(result[1]).toMatchObject(materialB);
    expect(result[2]).toMatchObject(materialC);
  });

  it('can fetch multiple materials with offset', async () => {
    await materials.create(materialA);
    await materials.create(materialB);
    await materials.create(materialC);
    const offset = 1;
    const result = await materials.findAll(offset);
    expect(result.length).toEqual(2);
    expect(result[0]).toMatchObject(materialB);
    expect(result[1]).toMatchObject(materialC);
  });

  it('can fetch multiple materials with offset and limit', async () => {
    await materials.create(materialA);
    await materials.create(materialB);
    await materials.create(materialC);
    const offset = 1;
    const limit = 1;
    const result = await materials.findAll(offset, limit);
    expect(result.length).toEqual(1);
    expect(result[0]).toMatchObject(materialB);
  });

  it('can query by name', async () => {
    await materials.create(materialA);
    await materials.create(materialB);
    await materials.create(materialC);

    const query = 'ore';
    const result = await materials.search(query);
    expect(result[0]).toMatchObject(materialA);
    expect(result[1]).toMatchObject(materialB);
  });

  it('can update by id', async () => {
    const material = await materials.create(materialA);
    const updatedName = 'updatedName';
    const result = await materials.updateById(material.get('id'), {
      name: updatedName,
    });
    expect(result.get('name')).toEqual(updatedName);
  });

  it('throws an error when trying to update a material that does not exist', async () => {
    const updatedName = 'updatedName';
    const nonExistingMaterialId = 123456;
    await expect(materials.updateById(nonExistingMaterialId, {
      name: updatedName,
    })).rejects.toThrowError(/not found/);
  });

  it('can remove a material by id', async () => {
    const material = await materials.create(materialA);
    await materials.destroyById(material.get('id'));
    const checkAgainForMaterial = await materials.findById(material.get('id'));
    expect(checkAgainForMaterial).toBeNull();
  });
});
