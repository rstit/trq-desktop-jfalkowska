const tmp = require('tmp');
const Database = require('../../database/Database');
const MaterialModel = require('../../materials/material.model');
const SequenceModel = require('../../sequences/sequence.model');
const SequenceProvicer = require('../../sequences/sequence.provider');

const SequenceService = require('../../sequences/sequence.service');

let sequences;
let materialModel;
let sequenceModel;
let database;
let sequenceService;

const database_location = tmp.tmpNameSync();

const setup = async () => {
  database = new Database(database_location);
  sequenceService = new SequenceService({ database });
  await database.authenticate();
  return await database.sync();
}

const tearDown = async () => {
  await database.getModel('material').drop();
  return await database.getModel('sequence').drop();
}

describe('sequences', () => {
  beforeEach(setup);
  afterEach(tearDown);

  let material;

  beforeEach(async () => {
    material = await sequenceService.materialModel.create({
      name: 'FakeMaterial',
    });
  });
  it('creates sequence', async () => {
    const result = await sequenceService.provider.create();
    expect(typeof result.id === 'string').toEqual(true);
  });
});
