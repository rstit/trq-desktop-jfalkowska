const tmp = require('tmp');
const fs = require('fs');
const Database = require('../../database/Database');

const MaterialProvider = require('../../materials/material.provider');
const SequenceProvider = require('../../sequences/sequence.provider');
const ScanProvider = require('../../scans/scan.provider');
const ScanningProcessService = require('../../scanningProcess/scanningProcess.service');

const materialModelDefinition = require('../../materials/material.model');
const sequenceModelDefinition = require('../../sequences/sequence.model');

const Exporter = require('../../exporter/Exporter');

let database;
let scanningService;
let exporter;
let materialProvider;
let scanProvider;
let sequenceProvider;

const partNumber = 'TRQ-123'
const manufacturer = 'Triteq';
const description = 'A scan';
const detectionDepth = 2.2;
const frequency = 5.234;
const additionalInfo = {};

const database_location = tmp.tmpNameSync();

const setup = async () => {
  database = new Database(database_location);

  materialProvider = new MaterialProvider({
    model: database.getModel('material'),
  });

  sequenceProvider = new SequenceProvider({
    model: database.getModel('sequence'),
  });

  scanProvider = new ScanProvider({
    model: database.getModel('scan'),
  });

  scanningService = new ScanningProcessService({
    scanProvider, sequenceProvider, materialProvider
  });

  await database.authenticate();

  return await database.sync();
}

const tearDown = async () => {
  await database.getModel('scan').drop();
  await database.getModel('material').drop();
  return await database.getModel('sequence').drop();
}

describe('exporting to CSV', () => {
  beforeEach(setup);
  afterEach(tearDown);
  it('exports all scans to JSON with default formatter', async () => {
    const exporter = new Exporter({ materialProvider, scanProvider });
    const material = await scanningService.materialProvider.create({ name: 'foo' });
    const sequence = await scanningService.createSequence();
    const scan0 = await scanningService.createScanInSequenceForMaterial({
      partNumber: `${partNumber}0`,
      manufacturer,
      description,
      detectionDepth,
      frequency,
      additionalInfo,
    }, sequence, material);
    const scan1 = await scanningService.createScanInSequenceForMaterial({
      partNumber: `${partNumber}1`,
      manufacturer,
      description,
      detectionDepth,
      frequency,
      additionalInfo,
    }, sequence, material);
    const exportedScans = await exporter.exportAllScans();
    expect(Array.isArray(exportedScans)).toEqual(true);
    expect(exportedScans[0]).toMatchObject(
      {
        partNumber: `${partNumber}1`,
        manufacturer,
        description,
        detectionDepth,
        frequency,
        additionalInfo: JSON.stringify(additionalInfo),
      }
    );
    expect(exportedScans[1]).toMatchObject(
      {
        partNumber: `${partNumber}0`,
        manufacturer,
        description,
        detectionDepth,
        frequency,
        additionalInfo: JSON.stringify(additionalInfo),
      }
    );
  });

  it('saves exported data to CSV', async () => {
    const header = 'partNumber';
    const partNumber0 = 'TRQ-0';
    const partNumber1 = 'TRQ-1';
    const outputPath = tmp.tmpNameSync();
    const exporter = new Exporter({ materialProvider, scanProvider });
    const exportedScans = [
      { partNumber: partNumber0 },
      { partNumber: partNumber1 }
    ];
    await exporter.saveToCSV(exportedScans, outputPath);
    const fileContent = fs.readFileSync(outputPath, 'UTF-8');
    expect(fileContent).toEqual(`"${header}"\n"${partNumber0}"\n"${partNumber1}"`);
  });
});
