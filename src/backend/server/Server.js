const MaterialProvider = require('../materials/material.provider');
const SequenceProvider = require('../sequences/sequence.provider');
const ScanProvider = require('../scans/scan.provider');

const ScanningProcessService = require('../scanningProcess/scanningProcess.service');
const Exporter = require('../exporter/Exporter');
const DeviceScanner = require('../device/DeviceScanner');
const DeviceClient = require('../communication/DeviceClient');

const faker = require('faker');
class Server {
  static resultToJSON(model) {
    return model.get({ plain: true });
  }
  constructor({
    wifi,
    database,
    ipc,
  }) {
    this.wifi = wifi;
    this.database = database;
    this.ipc = ipc;
    this.deviceScanner = new DeviceScanner();
    this.deviceConnection = null;
    this.client = null;
  }

  bindListeners() {
    this.addEventListenerOnIPC('materialFindAll', async ({ offset, limit, order }) => {
      const materials = await this.materials.findAll(offset, limit, order);
      return materials.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('materialSearch', async ({ query, offset, limit, order }) => {
      const materials = await this.materials.search(query, offset, limit, order);
      return materials.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('materialCreate', async (material) => {
      return Server.resultToJSON(await this.materials.create(material));
    });

    this.addEventListenerOnIPC('materialFindById', async (id) => {
      return Server.resultToJSON(await this.materials.findById(id));
    });

    this.addEventListenerOnIPC('materialUpdateById', async ({ id, material }) => {
      return Server.resultToJSON(await this.materials.updateById(id, material));
    });

    this.addEventListenerOnIPC('materialDestroyById', async (id) => {
      return Server.resultToJSON(await this.materials.destroyById(id));
    });

    this.addEventListenerOnIPC('createSequence', async () => {
      const sequence = await this.scanningProcess.createSequence();
      return sequence.id;
    });

    this.addEventListenerOnIPC('scanCreateInSequenceForMaterial', async ({ scanDefinition, sequenceId, materialId }) => {
      const materialModel = await this.materials.findById(materialId);
      const sequenceModel = await this.sequences.findById(sequenceId);
      const scanModel = await this.scanningProcess.scanCreateInSequenceForMaterial(scanDefinition, sequenceModel, materialModel);
      return Server.resultToJSON(scanModel);
    });

    this.addEventListenerOnIPC('scanFindRelated', async ({ scanId, offset, limit, order }) => {
      const scanModel = await this.scans.findById(scanId);
      const scans = await this.scanningProcess.scanFindRelated(scanModel, offset, limit, order);
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('saveScansToCSVFile', async ({ query, offset, limit, order }) => {
      const outputPath = await this.exporter.showSaveDialog();
      if (!outputPath) {
        return;
      }
      const exportedScans = await this.exporter.exportAllScans(query, offset, limit, order);
      try {
        await this.exporter.saveToCSV(exportedScans, outputPath);
      } catch(error) {
        // @TODO: add error handling in TRQDA-41
      }
    });

    this.addEventListenerOnIPC('scanFindById', async (id) => {
      return Server.resultToJSON(await this.scans.findById(id));
    });

    this.addEventListenerOnIPC('scanUpdateById', async ({ id, scan }) => {
      return Server.resultToJSON(await this.scans.updateById(id, scan));
    });

    this.addEventListenerOnIPC('scanDestroyById', async (id) => {
      return Server.resultToJSON(await this.scans.destroyById(id));
    });

    this.addEventListenerOnIPC('scanSearch', async ({ query, offset, limit, order }) => {
      const scans = await this.scans.search(query, offset, limit, order);
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('scanFindAll', async ({ offset, limit, order, where }) => {
      const scans = await this.scans.findAll(offset, limit, order, where);
      return scans.map(Server.resultToJSON);
    });

    this.addEventListenerOnIPC('wifiIsConnected', async () => {
      return await this.wifi.isConnected();
    });

    this.addEventListenerOnIPC('wifiGetCurrectdeviceConnections', async () => {
      return await this.wifi.getCurrentdeviceConnections();
    });

    this.addEventListenerOnIPC('wifiScan', async () => {
      return await this.wifi.scan();
    });

    this.addEventListenerOnIPC('sendDDSConfig', async (config) => {
      if (!this.deviceConnection) {
        return;
      }
      return await this.deviceConnection.sendDDSConfig(config);
    });

    this.connectToDevice();
  }

  installProxyEventBetweenDeviceAndClient(eventName) {
    if (!this.deviceConnection) {
      return;
    }
    this.deviceConnection.on(eventName, (...payload) => {
      this.client.send(eventName, ...payload);
    });
  }

  uninstallProxyEventBetweenDeviceAndClient(eventName) {
    if (!this.deviceConnection) {
      return;
    }
    this.deviceConnection.removeAllListeners(eventName);
  }

  async establishDeviceConnection() {
    if (!this.deviceConnection) {
      // First, we need to scan the network for a compatible device
      this.deviceConnection = await this.deviceScanner.findScanningDevice();
    }

    if (!this.deviceConnection) {
      return this.establishDeviceConnection();
    }

    this.deviceConnection.on('close', () => {
      if (this.client && !this.client.destroyed) {
        this.client.send('disconnected');
      }
      this.deviceConnection = null;
      this.establishDeviceConnection();
    });

    return this.deviceConnection;
  }

  async connectToDevice() {
    const onConnect = async (event, arg) => {
      this.client = event.sender;
      const connection = await this.establishDeviceConnection();
      if (!connection) {
        return;
      }
      this.installProxyEventBetweenDeviceAndClient('handshake');
      this.installProxyEventBetweenDeviceAndClient('disconnected');
      this.installProxyEventBetweenDeviceAndClient('battery');
      this.installProxyEventBetweenDeviceAndClient('measure');
      this.installProxyEventBetweenDeviceAndClient('scanner-alert');
      this.deviceConnection.sendHandShake();
    };

    const onDisconnect = async (event, arg) => {
      this.client = null;
      this.uninstallProxyEventBetweenDeviceAndClient('handshake');
      this.uninstallProxyEventBetweenDeviceAndClient('disconnected');
      this.uninstallProxyEventBetweenDeviceAndClient('battery');
      this.uninstallProxyEventBetweenDeviceAndClient('measure');
      this.uninstallProxyEventBetweenDeviceAndClient('scanner-alert');
    };

    this.on('client-connect', onConnect);

    // We need to remove all events listeners from device when the client disconnects.
    // Without this, there will be a memory leak and duplicated events on the client.
    this.on('client-disconnect', onDisconnect);
  }

  on(eventName, callback) {
    this.ipc.on(eventName, callback);
  }

  off(eventName, callback) {
    this.ipc.removeListener(eventName, callback);
  }

  addEventListenerOnIPC(eventName, callback) {
    this.on(eventName, async (event, arg) => {
      const result = await callback(arg);
      event.sender.send(eventName, result);
    });
  }

  async init() {
    this.materials = new MaterialProvider({
      model: this.database.getModel('material'),
    });
    this.sequences = new SequenceProvider({
      model: this.database.getModel('sequence'),
    });
    this.scans = new ScanProvider({
      model: this.database.getModel('scan'),
      materialModel: this.database.getModel('material')
    });
    this.scanningProcess = new ScanningProcessService({
      materialProvider: this.materials,
      sequenceProvider: this.sequences,
      scanProvider: this.scans,
    });

    this.exporter = new Exporter({
      scanProvider: this.scans,
      materialProvider: this.materials,
    });

    await this.database.sync();
    await this.database.migrate();
    await this.wifi.init();

    this.bindListeners();

    await this.createFakeMaterials();
    return await this.createFakesScans();
  }

  async createFakeMaterials() {
    const anyMaterials = await this.materials.findAll();
    if (anyMaterials.length) {
      return;
    }
    for (let i = 0; i < 100; ++i) {
      await this.materials.create({
        name: faker.lorem.word(),
        permeability: faker.random.number(),
        resistance: faker.random.number(),
        frequency: faker.random.number(),
      });
    }
  }

  async createFakesScans() {
    const anyScans = await this.scans.findAll();
    if (anyScans.length) {
      return;
    }
    const materials = await this.materials.findAll();
    const sequence = await this.scanningProcess.createSequence();
    for (let i = 0; i < 100; ++i) {
      const index = Math.round( Math.random() * (materials.length - 1))
      const material = materials[ index ];
      const scan = await this.scanningProcess.scanCreateInSequenceForMaterial({
        partNumber: `TRQ-${faker.random.number()}`,
        manufacturer: 'Triteq',
        description: faker.lorem.paragraphs(Math.random()),
        detectionDepth: faker.random.number(),
        frequency: faker.random.number(),
        additionalInfo: {
          foo: "bar"
        },
      }, sequence, material);
    }
  }
}

module.exports = Server;
