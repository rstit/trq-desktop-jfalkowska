const MaterialProvider = require('../materials/material.provider');
const SequenceProvider = require('../sequences/sequence.provider');
const ScanProvider = require('../scans/scan.provider');
const Sequelize = require('sequelize');

module.exports = class SequenceService {
  constructor({
    materialProvider,
    sequenceProvider,
    scanProvider,
  }) {
    this.materialProvider = materialProvider;
    this.sequenceProvider = sequenceProvider;
    this.scanProvider = scanProvider;

    // this.sequenceProvider.model.hasMany(this.scanProvider.model);
    // this.scanProvider.model.belongsTo(this.sequenceProvider.model);

    // this.materialProvider.model.hasMany(this.scanProvider.model);
    // this.scanProvider.model.belongsTo(this.materialProvider.model);
  }

  async createSequence() {
    return await this.sequenceProvider.create();
  }

  async scanCreateInSequenceForMaterial(scanDefinition, sequenceModel, materialModel) {
    const scanModel = await sequenceModel.createScan(scanDefinition);
    await materialModel.addScan(scanModel);
    return scanModel;
  }

  async scanFindRelated(scanModel, offset, limit, order) {
    const where = {
      sequenceId: scanModel.get('sequenceId'),
      id: {
        [Sequelize.Op.ne]: scanModel.get('id'),
      }
    };
    return await this.scanProvider.findAll(offset, limit, order, where);
  }
}
