const net = require('net');
const dns = require('dns');
const os = require('os');
const networkList = require('network-list');
const Client = require('./../communication/DeviceClient')
const { DEVICE_PORT } = require('../constants');

const defautlOptions = {};

module.exports = class DeviceScanner {
  /**
   * Returns a promise with array of found devices.
   * Those are all devices found on the network we are currently connected.
   * They can be scanners, but that is not guaranteed.
   *
   * Single device object:
   * {
   *   ip: '192.168.1.1', // IP address
   *   alive: false, // true/false - device is alive
   *   hostname: null, // string - dns reverse hostname
   *   mac: null, // string - MAC address
   *   vendor: null, // string - vendor name
   *   hostnameError: null, // Error message if got error on getting hostname
   *   macError: null, // Error message if got an error on getting Mac Address
   *   vendorError: null, // Error message if got an error on getting vendor name
   * }
   */
  async scan(options = {}) {
    return new Promise((resolve, reject) => {
      networkList.scan(Object.assign({}, defautlOptions, options), (err, network) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(network);
      });
    });
  }

  /**
   * Checks if on given host and port is a devices that is a TRQ Scanner.
   * If the device is a scanner then a promise is returned with opened socket
   * to that devices. Otherwise a promise with value of `false` is returned.
   * @param {number} port
   * @param {string} host
   */
  async isDeviceATRQScanner(port, host) {
      const client = new Client(port, host);
      return await client.connect();
  }

  /**
   * Returns a promise with array of found devices that are a TRQ Scanner.
   * If array is empty - no scanner found.
   */
  async findScanningDevice() {
    const network = await this.scan();
    const sockets = network
      .filter(device => device.alive)
      .map(device => {
          const port = DEVICE_PORT;
          const host = device.ip;
          return this.isDeviceATRQScanner(port, host);
      });
    return await Promise.race(sockets);
  }
}
