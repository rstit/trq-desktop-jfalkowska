const Sequelize = require('sequelize');
const SORT_ASC = 'ASC';
const Op = Sequelize.Op;

const modelToJSON = result => result.get({ plain: true });

class MaterialProvider {
  constructor({ model }) {
    this.model = model;
  }

  async search(query, offset, limit, order) {
    return await this.findAll(offset, limit, order, {
        name: {
          [Op.like]: `%${query}%`,
        }
    });
  }

  async findAll(offset = 0, limit = 100, order = SORT_ASC, where) {
    return await this.model.findAll({
      order: [
        ['name', order]
      ],
      offset,
      limit,
      where,
    });
  }

  async findById(id) {
    return await this.model.findById(id);
  }

  async create({
    name,
    permeability,
    resistance,
    frequency,
  }) {
    return await this.model.create({
      name,
      permeability,
      resistance,
      frequency,
    });
  }

  async updateById(id, fields) {
    const material = await this.model.findById(id);
    if (!material) {
      throw new Error(`Material with id ${id} not found`);
    }
    Object.assign(material, fields);
    return await material.save({
      fields: Object.keys(fields)
    });
  }

  async destroyById(id) {
    const material = await this.model.findById(id);
    if (!material) {
      throw new Error(`Material with id ${id} not found`);
    }
    return await material.destroy();
  }
}

module.exports = MaterialProvider;
