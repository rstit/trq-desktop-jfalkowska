const protocol = require('./protocol');

protocol.define('handshake', {
  write: function ({
    deviceId,
    firmwareVersion,
    deviceStatus,
  }) {
    this.Int32BE(deviceId)
      .Int32BE(firmwareVersion)
      .Int32BE(deviceStatus);
  },
  read: function () {
    this.Int32BE('deviceId')
        .Int32BE('firmwareVersion')
        .Int32BE('deviceStatus');
  }
});

protocol.define('batteryStatus', {
  write: function({
    batteryLevel,
  }) {
    this.Int16BE(batteryLevel);
  },

  read: function() {
    this.Int16BE('batteryLevel');
  }
});

protocol.define('batteryConfig', {
  write: function({
    fctc,
    rcomp0,
    tempCo,
    tempNom,
    tempLim,
    vEmpty,
    fullCapNom,
    lavgEmpty,
  }) {
    this.Int16BE(fctc)
        .Int16BE(rcomp0)
        .Int16BE(tempCo)
        .Int16BE(tempNom)
        .Int16BE(tempLim)
        .Int16BE(vEmpty)
        .Int16BE(fullCapNom)
        .Int16BE(lavgEmpty);
  },
  read: function() {
    this.Int16BE('fctc')
        .Int16BE('rcomp0')
        .Int16BE('tempCo')
        .Int16BE('tempNom')
        .Int16BE('tempLim')
        .Int16BE('vEmpty')
        .Int16BE('fullCapNom')
        .Int16BE('lavgEmpty');
  }
});

protocol.define('status', {
  write: function({
    flags,
  }) {
    this.Int32BE(flags);
  },

  read: function() {
    this.Int32BE('flags');
  }
});

protocol.define('ddsConfig', {
  write: function({
    frequencyTuning,
    sweepParameter1,
    sweepParameter2,
    risingDelta,
    fallingDelta,
    risingSweepRampRate,
    fallingSweepRampRate,
    phaseOffset,
  }) {
    this.Int32BE(frequencyTuning)
        .Int32BE(sweepParameter1)
        .Int32BE(sweepParameter2)
        .Int32BE(risingDelta)
        .Int32BE(fallingDelta)
        .Int32BE(risingSweepRampRate)
        .Int32BE(fallingSweepRampRate)
        .Int16BE(phaseOffset);
  },

  read: function() {
    this.Int32BE('frequencyTuning')
        .Int32BE('sweepParameter1')
        .Int32BE('sweepParameter2')
        .Int32BE('risingDelta')
        .Int32BE('fallingDelta')
        .Int32BE('risingSweepRampRate')
        .Int32BE('fallingSweepRampRate')
        .Int16BE('phaseOffset');
  }
});

protocol.define('attenuatorsConfig', {
  write: function({
    value0,
    value1,
    value2,
  }) {
    this.Int8(value0)
        .Int8(value1)
        .Int8(value2);
  },

  read: function() {
    this.Int8('value0')
        .Int8('value1')
        .Int8('value2');
  }
});

protocol.define('wifiConfig', {
  write: function({
    ssid,
    password,
    flags,
  }){
    this.Char32(ssid)
        .Char32(password)
        .Int16BE(flags);
  },
  read: function(){
    this.Char32('ssid')
        .Char32('password')
        .Int16BE('flags');
  }
});

protocol.define('Char32', {
  write: function (chars) {
    this.loop(chars, this.Int8);
  },
  read: function() {
    this.loop('chars', this.Int8, 32);
    return this.context.chars.map(char => {
      return String.fromCharCode(char);
    }).join('');
  }
});
