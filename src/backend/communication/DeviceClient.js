const { EventEmitter } = require('events');
const net = require('net');
const protocol = require('./protocol');
const TIMEOUT = 10000;

const {
  HANDSHAKE,
  BEEP_HEADER,
  BEEP_RESPONSE_HEADER,
  STATUS_OK,
  DDS_CONFIG_STRUCT,
  READ_DATA_HEADER,
  WRITE_DATA_HEADER,
  STATUS_WRONG_DATA_VALUES,
  MEASURE_DATA_HEADER,
  READ_DATA_RESPONSE_HEADER,
  WRITE_DATA_RESPONSE_HEADER,
  BATTERY_STATUS_STRUCT,
} = require('../constants');

class DeviceClient extends EventEmitter {
  constructor(port, host) {
    super();
    this.host = host;
    this.port = port;
    this.handshakeInfo = {
      name: null,
      deviceId: null,
      firmwareVersion: null,
      deviceStatus: null
    };
  }

  static createDDSConfig(config) {
    return protocol.write().writeData({
      struct_id: DDS_CONFIG_STRUCT,
      struct: config,
    }).result;
  }

  static createBeep() {
    return protocol.write().beep().result;
  }

  static createHandShake() {
    return protocol.write().readData({
      struct_id: HANDSHAKE
    }).result;
  }

  static readHeader(buffer) {
    return buffer[0];
  }

  static isHandShake(buffer) {
    if (DeviceClient.readHeader(buffer) === READ_DATA_RESPONSE_HEADER) {
      const { result } = protocol.read(buffer).readDataResponse();
      return result && result.struct_id === HANDSHAKE;
    }
    return false;
  }

  static isBeepSuccessful(buffer) {
    return DeviceClient.readHeader(buffer) === BEEP_RESPONSE_HEADER && protocol.read(buffer).beepResponse().result.status === STATUS_OK;
  }

  handleWriteResponseMessage(message) {
    switch (message.status) {
      case STATUS_WRONG_DATA_VALUES:
        this.emit('scanner-alert', {
          alertType: 'error',
          message: 'Invalid data has been sent to scanning device. Pleas check your setup.',
          code: message.status,
        });
        break;
    }
  }

  handleDataResponseMessage(message) {
    switch(message.struct_id) {
      case BATTERY_STATUS_STRUCT:
        this.emit('battery', message.struct.batteryLevel / 100);
        break;
      case HANDSHAKE:
        Object.assign(this.handshakeInfo, message.struct);
        this.emit('handshake', message.struct);
        break;
    }
  }

  handleMeasureMessage(message) {
    this.emit('measure', message);
  }

  handleIncomingData(buffer) {
      switch(DeviceClient.readHeader(buffer)) {
        case WRITE_DATA_RESPONSE_HEADER:
          this.handleWriteResponseMessage(protocol.read(buffer).writeDataResponse().result);
          break;
        case READ_DATA_RESPONSE_HEADER:
          this.handleDataResponseMessage(protocol.read(buffer).readDataResponse().result);
          break;
        case MEASURE_DATA_HEADER:
          this.handleMeasureMessage(protocol.read(buffer).measureData().result);
          break;
      }
  }

  connect() {
    const beep = DeviceClient.createBeep();
    this.socket = net.createConnection(this.port, this.host);
    this.socket.setTimeout(TIMEOUT);
    this.socket.on('data', this.handleIncomingData.bind(this));
    this.socket.on('close', () => {
      this.emit('close');
      this.socket.destroy();
    });

    // We are sending the beep message to see if the device will respond.
    this.socket.write(beep);

    return new Promise((resolve) => {
      this.socket.on('data', (buffer) => {
        if (DeviceClient.isBeepSuccessful(buffer)) {
          resolve(this);
        }
      });
      this.socket.on('timeout', () => {
        this.emit('close');
        this.socket.destroy();
      });
      this.socket.on('error', () => {
        this.emit('close');
        this.socket.destroy();
      });
    });
  }

  sendHandShake() {
    const handshake = DeviceClient.createHandShake();
    this.socket.write(handshake);
  }

  sendDDSConfig(config) {
    const ddsConfig = DeviceClient.createDDSConfig(config);
    this.socket.write(ddsConfig);
  }

  getDataFromLastHandshakeResponse() {
    return this.handshakeInfo;
  }
}

module.exports = DeviceClient;
