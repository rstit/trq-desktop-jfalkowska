const ipcRenderer = (<any>window).require('electron').ipcRenderer;
import { Observable } from 'rxjs/Observable';

const measureObservable = Observable.create((observer) => {
  ipcRenderer.on('measure', (event, measure) => {
    observer.next({ measure });
  });
});

export default measureObservable;
