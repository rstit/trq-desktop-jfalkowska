import { send } from '../lib/ipcAdapter';

/**
 * Sends IPC message to the main process which open a save dialog window.

 * @param offset
 * @param limit
 * @param order
 */
export const saveScansToCSVFile = async (query: string, offset: number = 0, limit?: number, order: string = 'ASC'): Promise<boolean> => {
  return send<boolean>('saveScansToCSVFile', {
    query,
    offset,
    limit,
    order,
  });
};
