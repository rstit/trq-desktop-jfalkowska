const ipcRenderer = (<any>window).require('electron').ipcRenderer;
import { Observable } from 'rxjs/Observable';


export enum ScannerStatusType {
  connected = 'connected',
  disconnected = 'disconnected'
}
export enum ScannerAlertType {
  error = 'error',
  warning = 'warning',
  success = 'success'
}
export enum ScannerMessageType {
  status,
  alert
}

export interface IScannerStatusData {
  state: ScannerStatusType;
  name?: string;
  battery?: number;
  deviceId?: number;
  firmwareVersion?: number;
  deviceStatus?: number;
}
export interface IScannerAlertData {
  alertType: ScannerAlertType;
  message: string;
  code?: string|number;
}
export interface IScannerMessage {
  messageType: ScannerMessageType;
  data: IScannerStatusData|IScannerAlertData;
}


/**
 * Observable which provides information about state of a device:
 * - if it connected or disconnected
 * - device name and battery state
 *
 * Value provided by the observable can consist of:
 * - state ('connected', 'disconnected)
 * - name - device name
 * - battery - power level; number from 0 to 1;
 *
 * When device is disconnected, no information about name and battery are sent.
 *
 * ```typescript
 * import scannerObservable from '../communication/scanner.observable';
 * scannerObservable.subscribe((value) => {
 *   switch (value.state) {
 *    case 'connected':
 *      console.log('scanned is connected');
 *      break;
 *    case 'disconnected':
 *      console.log('scanner is disconnected');
 *      break;
 *   }
 *   if (typeof value.name !== 'undefined') {
 *     console.log(`Scanner name: ${value.name}`);
 *   }
 *   if (typeof value.battery !== 'undefined') {
 *     console.log(`Battery: ${value.battery}`);
 *   }
 * });
 * ```
 */

export const scannerObservable: Observable<IScannerMessage> = Observable.create((observer) => {
  ipcRenderer.on('disconnected', () => {
    observer.next({
      messageType: ScannerMessageType.status,
      data: {
        state: ScannerStatusType.disconnected,
      }
    });
  });
  ipcRenderer.on('battery', (event, battery) => {
    observer.next({
      messageType: ScannerMessageType.status,
      data: { battery }
    });
  });
  ipcRenderer.on('handshake', (event, deviceInfo) => {
    observer.next({
      messageType: ScannerMessageType.status,
      data: {
        state: ScannerStatusType.connected,
      }
    });
    observer.next({
      messageType: ScannerMessageType.status,
      data: deviceInfo
    });
  });
  ipcRenderer.on('scanner-alert', (event, { alertType, code, message }) => {
    observer.next({
      messageType: ScannerMessageType.alert,
      data: {
        alertType,
        code,
        message
      }
    });
  });
});

export const connectToBackend = () => ipcRenderer.send('client-connect');

// Let's clean after ourselves.
window.onbeforeunload = () => {
  ipcRenderer.send('client-disconnect');
};
