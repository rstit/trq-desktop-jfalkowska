import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'trq-wifi-status',
  templateUrl: './wifi-status.component.html',
  styleUrls: ['./wifi-status.component.scss']
})
export class WifiStatusComponent implements OnInit {
  @Input() isConnected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
