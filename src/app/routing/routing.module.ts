import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { MaterialsShowComponent } from '../materials/materials-show/materials-show.component';
import { MaterialEditComponent } from '../materials/material-edit/material-edit.component';
import { MaterialNewComponent } from '../materials/material-new/material-new.component';

import { ConnectionComponent } from '../connection/connection.component';
import { ConfigureDeviceComponent } from '../connection/configure-device/configure-device.component';

import { ScansShowComponent } from '../scanning/scans/scans-show/scans-show.component';
import { ScanPreviewComponent } from '../scanning/scans/scan-preview/scan-preview.component';
import { ScanSetupComponent } from '../scanning/scan-setup/scan-setup.component';
import { ScanningProcessComponent } from '../scanning/scanning-process/scanning-process.component';
import { ScanEditComponent } from '../scanning/scans/scan-edit/scan-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ConnectionComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'connection',
    component: ConnectionComponent,
  },
  {
    path: 'materials',
    component: MaterialsShowComponent,
  },
  {
    path: 'new-material',
    component: MaterialNewComponent,
  },
  {
    path: 'edit-material/:id',
    component: MaterialEditComponent,
  },
  {
    path: 'scan-setup/:step',
    component: ScanSetupComponent
  },
  {
    path: 'scanning-process',
    component: ScanningProcessComponent
  },
  {
    path: 'scan-history',
    component: ScansShowComponent,
  },
  {
    path: 'scan-preview/:id',
    component: ScanPreviewComponent,
  },
  {
    path: 'scan-edit/:id',
    component: ScanEditComponent,
  },
  {
    path: 'configure-device',
    component: ConfigureDeviceComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class RoutingModule { }
