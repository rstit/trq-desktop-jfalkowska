import { IScanSetup } from '../communication/ScanningProcess.adapter';

const decode = (value: string): string => decodeURIComponent(atob(value));
const encode = (value: string): string => btoa(encodeURIComponent(value));

export const packScanSetup = ({
  materialId,
  detectionDepth,
  frequency,
  partNumber,
  description,
}: IScanSetup): IScanSetup => {
  return {
    materialId,
    detectionDepth,
    frequency,
    partNumber: encode(partNumber),
    description: encode(description),
  };
};

export const unpackScanSetup = ({
  materialId,
  detectionDepth,
  frequency,
  partNumber,
  description,
}: IScanSetup): IScanSetup => {
  return {
    materialId: +materialId,
    detectionDepth: +detectionDepth,
    frequency: +frequency,
    partNumber: decode(partNumber),
    description: decode(description),
  };
};
