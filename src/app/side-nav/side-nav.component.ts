import { Component } from '@angular/core';
import { ConnectionService } from '../connection/connection.service';

@Component({
  selector: 'trq-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent {
  open = false;

  constructor(private _connectionService: ConnectionService) { }

  toggleOpen() {
    this.open = !this.open;
  }

  getCurrentState() {
    return this._connectionService.scannerData.state;
  }

  getScannerName() {
    return this._connectionService.scannerData.name;
  }

  getScannerBatteryLevel() {
    return this._connectionService.scannerData.battery;
  }

  getScannerBatteryClassName() {
    return this._connectionService.batteryLevelClassNumber();
  }
}
