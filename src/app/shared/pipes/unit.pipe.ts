import { Pipe, PipeTransform } from '@angular/core';
import { UNITS } from '../constants';

@Pipe({
  name: 'unit'
})
export class UnitPipe implements PipeTransform {

  transform(value: string, unitType: string): any {
    const unit = UNITS[unitType] || '';
    return `${value} ${unit}`;
  }

}
