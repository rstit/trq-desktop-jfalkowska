export const UNITS = {
  resistance: 'Ω / cm',
  permeability: 'µ',
  detectionDepth: 'µm',
  frequency: 'Hz'
};

export const SCAN_SETUP_STORAGE_KEY = 'scan-setup';
