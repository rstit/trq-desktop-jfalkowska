/* the scrolled list must have appropriate max-height (so the element is scrolled instead of entire window) */

/* example usage in template: */
/* <div class="foo-list-wrapper"
  trqInfiniteScroll
  (loadMoreItems) = "infiniteScrollCallback($event)"
  [offset] = "offset"
  [limit] = "limit"
  [hasNextPage] = "hasNextPage"
> */

/* example getItems function with query: */
/*
  getItems(offset = this.offset, limit = this.limit, reload = true) {
    if (reload) { this.offset = 0; }

    const request = this.query ? scansAdapter.scanSearch(this.query, offset, limit) : scansAdapter.scanFindAll(offset, limit);

    return request
      .then(result => {
        this.hasNextPage = !!result.length;
        if (reload) {
          this.scansList = <IScan[]>result;
        }
        else {
          this.scansList = this.scansList.concat(<IScan[]>result);
        }
      })
      .catch(error => console.warn('Error when getting scans: ', error));
  }
} */

/* example callback function: */
/*
  infiniteScrollCallback(event) {
    return this.getItems(event.limit, event.offset, false)
    .then(data => {
      if (typeof(event.callback) === 'function') { event.callback(); }
      else { throw new Error('Infinite scroll callback is not a function or is not defined'); }
    });
  }
*/

import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[trqInfiniteScroll]'
})
export class InfiniteScrollDirective {

  isLoadingNewPage: boolean;

  @Input() offset = 0;
  @Input() limit: number;
  @Input() hasNextPage: boolean;

  @Output() loadMoreItems: EventEmitter<{ offset: number; limit: number; callback: any; }> = new EventEmitter();

  constructor(private _element: ElementRef) { }

  @HostListener('scroll') onScroll() {
    if (this.shouldLoadNextPage()) {
      this.isLoadingNewPage = true;
      this.offset += this.limit;
      this.loadData();
    }
  }

  shouldLoadNextPage() {
    const scrollOffset = this._element.nativeElement.scrollHeight - this._element.nativeElement.clientHeight - 100;
    return this.hasNextPage && this._element.nativeElement.scrollTop >= scrollOffset;
  }

  loadData() {
    this.loadMoreItems.emit({
      offset: this.offset,
      limit: this.limit,
      callback: () => {
        this.isLoadingNewPage = false;
      }
    });
  }
}
