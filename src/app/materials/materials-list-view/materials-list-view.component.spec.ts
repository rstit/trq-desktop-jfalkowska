import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialsListViewComponent } from './materials-list-view.component';

describe('MaterialsListViewComponent', () => {
  let component: MaterialsListViewComponent;
  let fixture: ComponentFixture<MaterialsListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialsListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialsListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
