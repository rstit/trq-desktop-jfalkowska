import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RoutingModule } from './routing/routing.module';

import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { SideNavComponent } from './side-nav/side-nav.component';

import { InputSearchComponent } from './shared/input-search/input-search.component';
import { UnitPipe } from './shared/pipes/unit.pipe';
import { InfiniteScrollDirective } from './shared/directives/infinite-scroll.directive';
import { MinDirective } from './shared/directives/min.directive';
import { MaxDirective } from './shared/directives/max.directive';

import { MaterialEditViewComponent } from './materials/material-edit-view/material-edit-view.component';
import { MaterialsListViewComponent } from './materials/materials-list-view/materials-list-view.component';
import { MaterialNewComponent } from './materials/material-new/material-new.component';
import { MaterialEditComponent } from './materials/material-edit/material-edit.component';
import { MaterialsShowComponent } from './materials/materials-show/materials-show.component';

import { DropdownComponent } from './shared/dropdown/dropdown.component';
import { FrequencyDropdownComponent } from './materials/frequency-dropdown/frequency-dropdown.component';

import { ScanSetupComponent } from './scanning/scan-setup/scan-setup.component';
import { ScanningProcessComponent } from './scanning/scanning-process/scanning-process.component';
import { ScanSetupService } from './scanning/scan-setup/scan-setup.service';

import { ScansListViewComponent } from './scanning/scans/scans-list-view/scans-list-view.component';
import { ScansShowComponent } from './scanning/scans/scans-show/scans-show.component';
import { ScanPreviewComponent } from './scanning/scans/scan-preview/scan-preview.component';
import { ScanEditComponent } from './scanning/scans/scan-edit/scan-edit.component';

import { ConnectionComponent } from './connection/connection.component';
import { ConnectionService } from './connection/connection.service';
import { ScannerConnectedComponent } from './connection/scanner-connected/scanner-connected.component';
import { ConfigureDeviceComponent } from './connection/configure-device/configure-device.component';
import { WifiStatusComponent } from './connection/wifi-status/wifi-status.component';

import { AlertComponent } from './shared/alert/alert.component';
import { AlertService } from './shared/alert/alert.service';

@NgModule({
  declarations: [
    AppComponent,

    HomeComponent,
    SideNavComponent,

    InputSearchComponent,
    UnitPipe,
    InfiniteScrollDirective,
    MinDirective,
    MaxDirective,

    MaterialEditViewComponent,
    MaterialsListViewComponent,
    MaterialNewComponent,
    MaterialEditComponent,
    MaterialsShowComponent,

    DropdownComponent,
    FrequencyDropdownComponent,

    ConnectionComponent,
    ScannerConnectedComponent,
    ConfigureDeviceComponent,
    WifiStatusComponent,

    ScanSetupComponent,
    ScanningProcessComponent,
    ScansListViewComponent,
    ScansShowComponent,
    ScanPreviewComponent,
    ScanEditComponent,
    AlertComponent,
  ],

  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule
  ],
  providers: [
    ScanSetupService,
    ConnectionService,
    AlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
