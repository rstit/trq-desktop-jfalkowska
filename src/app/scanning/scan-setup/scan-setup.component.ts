import { Component, Input, Output, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IMaterial } from '../../../communication/Materials.adapter';
import { IScanSetup, IDDSConfig } from '../../../communication/ScanningProcess.adapter';
import scanningProcess from '../../../communication/scanningProcess';
import materials from '../../../communication/materials';
import { packScanSetup } from '../../helpers';
import { ScanSetupService } from './scan-setup.service';

export enum SetupStep {
  material,
  detectionDepth,
  frequency,
  partNumber,
  description
}

@Component({
  selector: 'trq-scan-setup',
  templateUrl: './scan-setup.component.html',
  styleUrls: ['./scan-setup.component.scss']
})
export class ScanSetupComponent implements OnInit {
  public setupSteps = SetupStep;
  @Input() currentStep: SetupStep = SetupStep.material;
  totalSteps: number = Object.keys(SetupStep).length / 2;
  @Input() setup: IScanSetup = {
    materialId: null,
    detectionDepth: null,
    frequency: null,
    partNumber: null,
    description: null,
  };
  frequencyMultiplier = 1;
  displayedFrequency: number;
  query: string;
  editMode = false;
  materialsArray: IMaterial[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _scanSetupService: ScanSetupService
  ) { }

  static packScanSetupForURLTransfer(setup: IScanSetup): IScanSetup {
    return packScanSetup(setup);
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.currentStep = +params['step'];
      this.handleStepChange();
    });
    // if we are first time on the screen, we want to have a clean slate.
    if (this.currentStep === SetupStep.material) {
      this.clearSetupStorage();
    // In any other case, it means we are coming back by using the "back button"
    // that means we should restore previously saved setup.
    } else {
      this.restoreSetup();
    }
  }

  getCurrentStepInfo() {
    return `Step ${this.getCurrentStepIndex() + 1}/${this.getTotalSteps()}`;
  }

  canChangeStep() {
    switch (this.currentStep) {
      case SetupStep.material:
        return this.setup.materialId !== null;
      case SetupStep.detectionDepth:
        return this.setup.detectionDepth !== null;
      case SetupStep.frequency:
        return this.setup.frequency !== null;
      case SetupStep.partNumber:
        return typeof this.setup.partNumber === 'string' && this.setup.partNumber.length > 0;
      case SetupStep.description:
        return typeof this.setup.description === 'string' && this.setup.description.length > 0;
      default:
        return true;
    }
  }

  isInEditMode() {
    return this.editMode;
  }

  editModeOff() {
    this.editMode = false;
  }

  editModeOn() {
    this.editMode = true;
  }

  isStep(stepName: SetupStep) {
    return SetupStep[stepName] === SetupStep[this.currentStep];
  }

  getTotalSteps() {
    return this.totalSteps;
  }

  getCurrentStepIndex() {
    return this.currentStep;
  }

  nextStep() {
    if (!this.canChangeStep()) {
      return;
    }
    if (this.isFinalStep()) {
      return;
    }
    this._router.navigate(['scan-setup', this.currentStep + 1]);
  }

  handleStepChange() {
    switch (this.currentStep) {
      case SetupStep.frequency:
        this.setFrequencyFromMaterial(this.setup.materialId);
        return;
      case SetupStep.partNumber:
        this.editModeOff();
        this.sendDDSConfig({
          frequencyTuning: this.getRecalculatedFrequency(),
        });
        return;
    }
  }

  isFinalStep() {
    return this.currentStep === this.totalSteps - 1;
  }

  getRecalculatedFrequency() {
    return this.setup.frequency * this.frequencyMultiplier;
  }

  submit() {
    this.saveSetup();
    this.nextStep();
    if (this.isFinalStep()) {
      this.setup.frequency = this.getRecalculatedFrequency();
      const packedSetup = ScanSetupComponent.packScanSetupForURLTransfer(this.setup);
      this._router.navigate(['scanning-process', packedSetup]);
    }
  }

  saveSetup() {
    this._scanSetupService.save(this.setup);
  }

  restoreSetup() {
    Object.assign(this.setup, this._scanSetupService.restore());
  }

  clearSetupStorage() {
    this._scanSetupService.clear();
  }

  async setFrequencyFromMaterial(materialId: number) {
    const materialModel = await materials.findById(materialId);
    if (materialModel) {
      this.setup.frequency = materialModel.frequency;
    }
  }

  sendDDSConfig(config: IDDSConfig) {
    scanningProcess.sendDDSConfig(config);
  }

  frequencyMultiplierChange(newFrequencyMultiplier) {
    this.frequencyMultiplier = newFrequencyMultiplier;
  }
}
