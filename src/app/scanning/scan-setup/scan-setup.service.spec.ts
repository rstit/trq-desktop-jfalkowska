import { TestBed, inject } from '@angular/core/testing';

import { ScanSetupService } from './scan-setup.service';

describe('ScanSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScanSetupService]
    });
  });

  it('should be created', inject([ScanSetupService], (service: ScanSetupService) => {
    expect(service).toBeTruthy();
  }));
});
