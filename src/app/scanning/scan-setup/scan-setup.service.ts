import { Injectable } from '@angular/core';
import { IScanSetup } from '../../../communication/ScanningProcess.adapter';

import { SCAN_SETUP_STORAGE_KEY } from '../../shared/constants';

@Injectable()
export class ScanSetupService {

  constructor() {}

  save(setup: IScanSetup) {
    localStorage.setItem(SCAN_SETUP_STORAGE_KEY, JSON.stringify(setup));
  }

  restore(): IScanSetup {
    return JSON.parse(localStorage.getItem(SCAN_SETUP_STORAGE_KEY));
  }

  clear() {
    localStorage.removeItem(SCAN_SETUP_STORAGE_KEY);
  }
}
