import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IScan } from '../../../../communication/ScanningProcess.adapter';

@Component({
  selector: 'trq-scans-list-view',
  templateUrl: './scans-list-view.component.html',
  styleUrls: ['../scans-list.scss']
})
export class ScansListViewComponent {
  @Input() scansList: IScan[];

  @Input() offset: number;
  @Input() limit: number;
  @Input() hasNextPage: boolean;

  @Output() scanSelected: EventEmitter<IScan> = new EventEmitter();
  @Output() scansListInfiniteScrollCallback: EventEmitter<any> = new EventEmitter();

  constructor() { }

  infiniteScrollCallback(event) {
    this.scansListInfiniteScrollCallback.emit(event);
  }
}
