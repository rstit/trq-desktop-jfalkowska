import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IScan } from '../../../../communication/ScanningProcess.adapter';
import scansAdapter from '../../../../communication/scanningProcess';


@Component({
  selector: 'trq-scan-edit',
  templateUrl: './scan-edit.component.html',
  styleUrls: ['./scan-edit.component.scss']
})
export class ScanEditComponent implements OnInit {
  scanId: number;
  scan: IScan = {
    id: null,
    partNumber: null,
    manufacturer: null,
    description: null,
    detectionDepth: null,
    frequency: null,
    additionalInfo: {},
    material: null
  };
  manufacturerVisible: boolean;
  additionalInfoArrays: [string, string][] = [];

  constructor(private route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.scanId = params['id'];
      this.getScan();
    });
  }

  getScan() {
    scansAdapter.scanFindById(this.scanId)
      .then(result => {
        this.scan = result;
        this.manufacturerVisible = !!this.scan.manufacturer;
        this.additionalInfoArrays = Object.entries(result.additionalInfo);
      })
      .catch(error => console.warn('Error while getting scan with id ${scanId} : ', error));
  }

  deleteManufacturer() {
    this.scan.manufacturer = null;
    this.manufacturerVisible = false;
  }
  addManufacturer() {
    this.manufacturerVisible = true;
  }

  addAdditionalInfo() {
    this.additionalInfoArrays.push(['', '']);
  }
  changeAdditionalInfo(newValue, additionalInfo: [string, string], index: number) {
    additionalInfo[index] = newValue;
  }
  deleteAdditionalInfoByIndex(index) {
    this.additionalInfoArrays.splice(index, 1);
  }
  isAdditionalInfoValid() {
    return !this.additionalInfoArrays.find(additionalInfo => !additionalInfo[0] || !additionalInfo[1]);
  }

  getAdditionalInfoObject() {
    const additionalInfoObject = {};
    this.additionalInfoArrays.forEach(array => {
      additionalInfoObject[array[0]] = array[1];
    });
    return additionalInfoObject;
  }
  saveScan() {
    this.scan.additionalInfo = this.getAdditionalInfoObject();
    scansAdapter.scanUpdateById(this.scanId, this.scan)
      .then(result => this._router.navigate(['/scan-history']))
      .catch(error => console.warn('Error while deleting scan with id ${materialId} : ', error));
  }

  deleteScan() {
    scansAdapter.scanDestroyById(this.scanId)
      .then(result => this._router.navigate(['/scan-history']))
      .catch(error => console.warn('Error while deleting scan with id ${materialId} : ', error));
  }

}
