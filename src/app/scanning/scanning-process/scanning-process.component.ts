import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import { IScan, IScanSetup, IMeasurement } from '../../../communication/ScanningProcess.adapter';
import materials from '../../../communication/materials';
import { IMaterial } from '../../../communication/Materials.adapter';
import { unpackScanSetup } from '../../helpers';
import measure from '../../../communication/measure.observable';

const SCANNING_TIMEOUT_MS = 250;

@Component({
  selector: 'trq-scanning-process',
  templateUrl: './scanning-process.component.html',
  styleUrls: ['./scanning-process.component.scss']
})
export class ScanningProcessComponent implements OnInit, OnDestroy {
  private _measureSubscription: Subscription;
  setup$: Observable<IScan[]>;
  scanTemplate: IScan = {
    id: null,
    frequency: null,
    detectionDepth: null,
    description: null,
    partNumber: null,
  };
  material: IMaterial = {
    name: null,
    permeability: null,
    resistance: null,
    frequency: null,
    createdAt: null,
    updatedAt: null,
  };
  measureInProgress = false;
  timeout: number;
  scans: IScan[] = [];
  measurements: IMeasurement[] = [];
  latestScan: IScan;

  static getCurrentDate() {
    return (new Date()).toString();
  }

  static calculateTimestamp(deviceTimestamp, measurementDelta): number {
    return deviceTimestamp + measurementDelta;
  }

  constructor(private _ngZone: NgZone, private route: ActivatedRoute) {
    this._measureSubscription = measure.subscribe((value) => {
      this._ngZone.run(() => {
        this.measureInProgress = true;
        this.saveMeasure(value.measure);
        clearTimeout(this.timeout);
        this.timeout = window.setTimeout(() => {
          this.measureInProgress = false;
          this.createNewScanFromTemplate();
        }, SCANNING_TIMEOUT_MS);
      });
    });
  }

  static unpackScanSetupFromURLTransfer(setup: IScanSetup): IScanSetup {
    return unpackScanSetup(setup);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.setScanFromParams(params);
    });
  }

  ngOnDestroy() {
    this._measureSubscription.unsubscribe();
  }

  async setScanFromParams(params) {
     const setup = ScanningProcessComponent.unpackScanSetupFromURLTransfer({
      materialId: params.get('materialId'),
      detectionDepth: params.get('detectionDepth'),
      frequency: params.get('frequency'),
      partNumber: params.get('partNumber'),
      description: params.get('description'),
    });

    const material = await materials.findById(setup.materialId);
    this.material = material;
    this.scanTemplate.material = material;
    this.scanTemplate.detectionDepth = setup.detectionDepth;
    this.scanTemplate.frequency = setup.frequency;
    this.scanTemplate.partNumber = setup.partNumber;
    this.scanTemplate.description = setup.description;
    const date = ScanningProcessComponent.getCurrentDate();
    this.scanTemplate.updatedAt = date;
    this.scanTemplate.createdAt = date;
    this.latestScan = this.scanTemplate;
    this.createNewScanFromTemplate();
  }

  createNewScanFromTemplate() {
    const newScan = <IScan>{};
    Object.assign(newScan, this.scanTemplate);
    newScan.partNumber = this.getNextPartNumber();
    newScan.data = this.measurements;
    const date = ScanningProcessComponent.getCurrentDate();
    newScan.createdAt = date;
    newScan.updatedAt = date;
    this.measurements = [];
    this.scans.push(newScan);
    this.latestScan = newScan;
  }

  getNextPartNumber(): string {
    const { partNumber } = this.scanTemplate;
    const index = this.scans.length;
    if (index === 0) {
      return partNumber;
    }
    return `${partNumber} (${index})`;
  }

  getCurrentScanIndex() {
    return this.scans.length - 1;
  }

  getLatestScan() {
    return this.latestScan || this.scanTemplate || <IScan>{};
  }

  saveMeasure({
    delta,
    timestamp,
    type,
    values
  }) {
    values.map((value) => {
      return <IMeasurement>{
        timestamp: ScanningProcessComponent.calculateTimestamp(timestamp, delta),
        type,
        value,
      };
    }).forEach((item) => {
      this.measurements.push(item);
    });
  }
}
